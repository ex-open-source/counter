defmodule Counter.Worker do
  use GenServer

  alias Counter.DynamicSupervisor, as: CounterSupervisor

  def handle_info(:work, %{count: count} = state) do
    new_state = %{state | count: count + 1}

    if count == 100 do
      DynamicSupervisor.terminate_child(CounterSupervisor, self())
      {:stop, :normal, new_state}
    else
      Process.send_after(self(), :work, 100)
      {:noreply, new_state}
    end
  end

  def init(state), do: {:ok, state}

  def start_link(opts), do: GenServer.start_link(__MODULE__, opts)
end
