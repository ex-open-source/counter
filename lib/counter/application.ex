defmodule Counter.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  alias Counter.DynamicSupervisor, as: CounterSupervisor

  def start(_type, _args) do
    opts = [strategy: :one_for_one, name: CounterSupervisor]
    DynamicSupervisor.start_link(opts)
  end
end
