defmodule Counter do
  alias Counter.DynamicSupervisor, as: CounterSupervisor
  alias Counter.Worker

  def work do
    {:ok, pid} = DynamicSupervisor.start_child(CounterSupervisor, {Worker, %{count: 0}})
    Process.send(pid, :work, [:noconnect])
  end
end
