defmodule Counter.MixProject do
  use Mix.Project

  def application, do: [mod: {Counter.Application, []}]

  def project, do: [app: :counter, elixir: "~> 1.7", version: "1.0.0"]
end
