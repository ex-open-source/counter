defmodule CounterTest do
  use ExUnit.Case

  alias Counter.DynamicSupervisor, as: CounterSupervisor

  test "children count" do
    IO.puts("At start there should be no workers started.")
    assert %{active: 0, specs: 0, supervisors: 0, workers: 0} == count_children()

    IO.puts("Starting first worker …")
    Counter.work()
    assert %{active: 1, specs: 1, supervisors: 0, workers: 1} == count_children()

    IO.puts("Starting second worker after 100 ms …")
    Process.sleep(100)
    Counter.work()
    assert %{active: 2, specs: 2, supervisors: 0, workers: 2} == count_children()

    IO.puts("First worker should be finished after 10 000 ms …")
    Process.sleep(10_000)
    assert %{active: 1, specs: 1, supervisors: 0, workers: 1} == count_children()

    IO.puts("Now second worker should be finished after 100 ms …")
    Process.sleep(100)
    assert %{active: 0, specs: 0, supervisors: 0, workers: 0} == count_children()

    IO.puts("Finally DynamicSupervisor should show 0 count …")
  end

  defp count_children, do: DynamicSupervisor.count_children(CounterSupervisor)
end
